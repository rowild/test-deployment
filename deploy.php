<?php

namespace Deployer;

// require 'recipe/common.php';

// inventory('.hosts.yaml');

// // Project name
// set('application', 'test-deployment');

// // Project repository
// set('repository', '');

// // [Optional] Allocate tty for git clone. Default value is false.
// set('git_tty', true);

// // Shared files/dirs between deploys
// set('shared_files', []);
// set('shared_dirs', []);

// // Writable dirs by web server
// set('writable_dirs', []);

// // Hosts

// host('test-deployment.rowild.at')
// 	->set('deploy_path', '~/{{application}}');

// // Tasks

// desc('Deploy your project');
// task('deploy', [
// 	'deploy:info',
// 	'deploy:prepare',
// 	'deploy:lock',
// 	'deploy:release',
// 	'deploy:update_code',
// 	'deploy:shared',
// 	'deploy:writable',
// 	'deploy:vendors',
// 	'deploy:clear_paths',
// 	'deploy:symlink',
// 	'deploy:unlock',
// 	'cleanup',
// 	'success'
// ]);

//// [Optional] If deploy fails automatically unlock.
// after('deploy:failed', 'deploy:unlock');

task('test', function () {
    writeln('Hello world');
});

// A default_stage MUST be set - NOT explained in Docu!
set('default_stage', 'production');

host('test-deployment.rowild.at')
    ->stage('production')
    ->set('deploy_path', '/www/htdocs/w00c8d25/test-deployment.rowild.at')
;

task('pwd', function () {
    $result = run('pwd');
    writeln("Current dir: {$result}");
});
