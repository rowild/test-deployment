module.exports = {
	"env": {
		"browser": true,
		"es2021": true
	},
	"extends": "eslint:recommended",
	"parserOptions": {
		"ecmaVersion": 12
	},
	"rules": {
		"no-console": "off",
		"quotes": "off",
		"no-undef": "off",
		"semi": ["warn", "always"],
		// https://eslint.org/docs/rules/indent
		"indent": [
			"error",
			"tab",
			{
				"ArrayExpression": "first",
				"ObjectExpression": "first",
				"ImportDeclaration": "first",
				"MemberExpression": 1,
				"outerIIFEBody": 0,
				"SwitchCase": 1,
				"VariableDeclarator": "first",
				"flatTernaryExpressions": false,
				"offsetTernaryExpressions": true,
				"ignoreComments": false
			}
		]
	}
};
